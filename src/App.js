import { BrowserRouter, Switch, Route } from "react-router-dom";
import AllPokemonComponent from "./components/AllPokemonComponent";
import "./App.css";
import React from "react";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/" component={AllPokemonComponent} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
