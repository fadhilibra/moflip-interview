import { Component } from "react";
import axios from "axios";

class AllPokemonComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pokemons: [],
      visiable: 1,
      //   pokemonData: [],
      //   offset: 0,
      //   perPage: 1,
      //   currentPage: 0,
      //   pageCount: 0,
    };

    this.loadNext = this.loadNext.bind(this);
  }

  componentDidMount() {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon`)
      .then((res) => {
        // console.log(res.data);
        // var data = res.data.results;
        // var slice = data.slice(this.state.offset, this.state.offset + this.state.perPage);
        this.setState({
          pokemons: res.data.results,
          // pokemonData: slice,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  loadNext() {
    this.setState((old) => {
      return { visiable: old.visiable + 1 };
    });
    // const data = this.state.pokemons;
    // const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage);
    // this.setState({
    //   pokemonData: slice,
    // });
  }

  //   handleClick = (e) => {
  //     const selectedPage = e.selected;
  //     const offset = selectedPage * this.state.perPage;

  //     this.setState(
  //       {
  //         currentPage: selectedPage,
  //         offset: offset,
  //       },
  //       () => {
  //         this.loadMoreData();
  //       }
  //     );
  //   };

  render() {
    return (
      <div className="container">
        <div>
          {this.state.pokemons.slice(0, this.state.visiable).map((pokemon) => (
            <p>{pokemon.name}</p>
          ))}
        </div>
        <div className="col-md-12 text-center mb-4">
          {this.state.visiable < this.state.pokemons.length && (
            <button type="button" onClick={this.loadNext} className="btn btn-primary">
              Load Next
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default AllPokemonComponent;
